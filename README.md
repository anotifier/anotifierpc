# _ANotifierPC_

Сервер для ANotifier.

## Установка:

* Установить Java (с поддержкой JavaFX)
* Скачать последний артефакт по
  ссылке: https://gitlab.com/anotifier/anotifierpc/-/jobs/artifacts/master/download?job=build
* Распаковать архив (желательно в папку без кириллицы)
* Запустить anotifierpc.jar двойным кликом ЛКМ или командой `java -jar anotifier.jar`

## Использование:

* При первом запуске необходимо поменять ключи шифрования на свои (поля Server Key и Client Key)
* Запустить демон нажатием кнопки Start

## Остальное:

* ANotifier: https://gitlab.com/anotifier/anotifierclient
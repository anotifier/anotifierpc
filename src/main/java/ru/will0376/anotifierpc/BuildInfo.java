package ru.will0376.anotifierpc;

public class BuildInfo {
	public static final String version = "DEV";
	public static final String buildTimestamp = "DEV";

	public String toString() {
		return "version         : " + version + "\n" + "build timestamp : " + buildTimestamp + "\n";
	}
}

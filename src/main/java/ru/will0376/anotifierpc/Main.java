package ru.will0376.anotifierpc;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.will0376.anotifierpc.controllers.Controllers;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.utils.Tray;
import ru.will0376.anotifierpc.utils.config.Config;

import java.io.File;

public class Main extends Application {
	public static Scene scene;
	public static Stage primaryStage;
	public static Config config;
	public static String version = BuildInfo.version;

	public static void main(String[] args) {
		launch(args);
	}

	public static boolean runningFromIntelliJ() {
		String classPath = System.getProperty("java.class.path");
		return classPath.contains("idea_rt.jar");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		File file = new File(System.getProperty("user.home"), "ANotifierPC");
		file.mkdir();
		config = Config.load(new File(file, "config.json"));
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(ClassLoader.getSystemResource("assets/fxml/Main.fxml"));
		Parent root = fxmlLoader.load();
		primaryStage.setTitle("ANotifierPC");
		primaryStage.setScene(new Scene(root));
		primaryStage.getIcons().add(Controllers.logo);
		scene = primaryStage.getScene();
		primaryStage.setResizable(false);
		Platform.setImplicitExit(false);
		primaryStage.show();
		Main.primaryStage = primaryStage;
	}

	@Override
	public void stop() throws Exception {
		//super.stop();
		if (!Tray.instance.minimize) {
			MainController.instance.stopDaemon();
			System.out.println("Stopped all!");
			Runtime.getRuntime().halt(0);
		}
	}
}

package ru.will0376.anotifierpc.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.apache.commons.lang3.math.NumberUtils;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.net.ANPCBootStrap;
import ru.will0376.anotifierpc.net.NetStatusData;
import ru.will0376.anotifierpc.utils.ExceptionHandler;
import ru.will0376.anotifierpc.utils.TextDebugLog;
import ru.will0376.anotifierpc.utils.Tray;
import ru.will0376.anotifierpc.utils.TrayNotificationHelper;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MainController extends ControllerInit {
	public static MainController instance;
	public static NetStatusData netStatusData = new NetStatusData();
	public ANPCBootStrap bootStrap;
	public Thread bootStrapThread;
	public ArrayList<TextDebugLog> debugLog = new ArrayList<>();
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
	public boolean dirtyDebugLog = false;
	@FXML
	public Text connectionsCount;
	public ScrollPane logScroll;
	public CheckBox autoScroll;
	public TextField shutTimer;
	@FXML
	TextFlow log;
	@FXML
	Text daemonStatus;
	@FXML
	TextField port;
	@FXML
	TextField clientKey;
	@FXML
	TextField serverKey;
	@FXML
	Text ipField;
	@FXML
	CheckBox autoStartSwitch;

	public static void appendToLog(String text, LogStatus... status) {
		appendToLogColor(text, Color.BLACK, status);
	}

	public static void appendToLogColor(String text, Color colorIn, LogStatus... status) {
		Platform.runLater(() -> {
			Text textInside = new Text(String.format("%s -> (%s) %s\n", timeFormat.format(Calendar.getInstance()
					.getTime()), (status.length >= 1 ? Arrays.stream(status)
					.map(Enum::name)
					.collect(Collectors.joining(", ")) : ""), text));
			textInside.setFill(colorIn);
			instance.log.getChildren().add(textInside);
		});
		appendToDebugLogColor(text, colorIn, status);
	}

	public static void appendToDebugLogColor(String text, Color colorIn, LogStatus... status) {
		String textInside = String.format("%s (%s) [%s]-> %s\n", timeFormat.format(Calendar.getInstance()
				.getTime()), (status.length >= 1 ? Arrays.stream(status)
				.map(Enum::name)
				.collect(Collectors.joining(", ")) : ""), Thread.currentThread().getName(), text);
		instance.debugLog.add(TextDebugLog.builder().color(colorIn).text(textInside).build());
		instance.dirtyDebugLog = true;
	}

	public static void appendToDebugLog(String text, LogStatus... status) {
		appendToDebugLogColor(text, Color.BLACK, status);
	}

	public static Map<byte[], byte[]> encryptText(SecretKey key, String in) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		AlgorithmParameters params = cipher.getParameters();
		byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
		byte[] ciphertext = cipher.doFinal(in.getBytes(StandardCharsets.UTF_8));
		return new HashMap<byte[], byte[]>() {{
			put(iv, ciphertext);
		}};
	}

	public static String decryptText(SecretKey key, byte[] encData, byte[] iv) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		return new String(cipher.doFinal(encData), StandardCharsets.UTF_8);
	}

	public void checkDaemon() {
		if (bootStrap != null && bootStrapThread != null && bootStrapThread.isAlive()) {
			daemonStatus.setText("Started");
			daemonStatus.setFill(Color.GREEN);
		} else {
			daemonStatus.setText("Stopped");
			daemonStatus.setFill(Color.RED);
		}
		try {
			ipField.setText(String.join(", ", getHostAddresses()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String[] getHostAddresses() {
		Set<String> HostAddresses = new HashSet<>();
		try {
			for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
				if (!ni.isLoopback() && ni.isUp() && ni.getHardwareAddress() != null) {
					for (InterfaceAddress ia : ni.getInterfaceAddresses()) {
						if (ia.getBroadcast() != null) {  //If limited to IPV4
							HostAddresses.add(ia.getAddress().getHostAddress());
						}
					}
				}
			}
		} catch (SocketException ignored) {
		}
		return HostAddresses.toArray(new String[0]);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
		instance = this;
		checkDaemon();
		port.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				port.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});

		port.setOnAction(e -> changedTextFields());
		clientKey.setOnAction(e -> changedTextFields());
		serverKey.setOnAction(e -> changedTextFields());
		autoStartSwitch.setOnAction(e -> changedTextFields());

		connectionsCount.setOnMouseClicked(e -> openDeviceNameList());

		postInit();
		new Tray().newTray();
	}

	public void postInit() {
		port.setText(String.valueOf(config.getPort()));
		clientKey.setText(config.getClientSecretKey());
		serverKey.setText(config.getServerSecretKey());

		boolean autoStart = config.isAutoStart();
		autoStartSwitch.setSelected(autoStart);
		if (autoStart) {
			appendToLog("AutoStarting daemon", LogStatus.Daemon);
			startDaemon();
		}
	}

	public void clearLog() {
		log.getChildren().clear();
	}

	public void exit() {
		Platform.exit();
	}

	public void openDeviceNameList() {
		Controllers.DeviceNameList.open();
	}

	public void openPacketLog() {
		Controllers.PacketLog.open();
	}

	public void openBlackList() {
		Controllers.NotificationFinder.open();
	}

	public void startDaemon() {
		checkDaemon();
		if (bootStrap != null && bootStrapThread != null && bootStrapThread.isAlive()) {
			appendToLog("Already started!", LogStatus.Daemon);
			return;
		}
		changedTextFields();
		bootStrap = new ANPCBootStrap(Integer.parseInt(port.getText()));
		bootStrapThread = new Thread(bootStrap);
		bootStrapThread.start();
		checkDaemon();
	}

	public void stopDaemon() {
		checkDaemon();
		if (bootStrap != null && bootStrapThread != null && bootStrapThread.isAlive()) {
			bootStrap.stopAll();
			bootStrapThread = null;
			bootStrap = null;
		} else {
			appendToLog("Not started", LogStatus.Daemon);
		}
		checkDaemon();
	}

	/**
	 * Используется для шифрования данных, идущих НА сервер
	 */
	public SecretKey getServerEncKey() throws Exception {
		return getEncKey(serverKey.getText());
	}

	/**
	 * Используется для шифрования данных, идущих С сервера
	 */
	public SecretKey getClientEncKey() throws Exception {
		return getEncKey(clientKey.getText());
	}

	public SecretKey getEncKey(String in) throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec spec = new PBEKeySpec(in.toCharArray(), in.getBytes(StandardCharsets.UTF_8), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		return new SecretKeySpec(tmp.getEncoded(), "AES");
	}

	public void changedTextFields() {
		config.setPort(Integer.parseInt(port.getText()));
		config.setClientSecretKey(clientKey.getText());
		config.setServerSecretKey(serverKey.getText());
		config.setAutoStart(autoStartSwitch.isSelected());
		appendToLog("Saved!", LogStatus.Config);
		config.saveConfig();
	}

	public void openDebugLog() {
		Controllers.DebugLogController.open();
	}

	public void openDecrypt() {
		Controllers.DecodeEncryptedDataController.open();
	}

	public void openB64Decoder() {
		Controllers.Base64Decoder.open();
	}

	public void copyLog() {
		StringBuilder builder = new StringBuilder();
		for (Node child : log.getChildren()) {
			Text child1 = (Text) child;
			builder.append(child1.getText());
		}
		Controllers.ControllerStorage controllerStorage = Controllers.CopyValueController.open();
		CopyValueController instance = (CopyValueController) controllerStorage.getInstance();
		instance.text.setText(builder.toString());
	}

	public void startShutTimer(ActionEvent actionEvent) {
		String text = shutTimer.getText();
		if (NumberUtils.isCreatable(text)) {
			if (TrayNotificationHelper.canShow()) {
				TrayNotificationHelper.shutTime = System.currentTimeMillis() + ((long) Integer.parseInt(text) * 1000 * 60);
				appendToLogColor(String.format("Silence timer set to %s", timeFormat.format(TrayNotificationHelper.shutTime)), Color.DARKGREEN, LogStatus.Notification, LogStatus.ShutUp);
			} else {
				TrayNotificationHelper.shutTime = 0;
				appendToLogColor("Silence timer disabled", Color.DARKGREEN, LogStatus.Notification, LogStatus.ShutUp);
			}
		}
	}

	public void openAbout(ActionEvent actionEvent) {
		Controllers.About.open();
	}

	public void openFormatter(ActionEvent actionEvent) {
		Controllers.NotificationDefaultFormatting.open();
	}

	public enum LogStatus {
		Netty,
		Config,
		Daemon,
		NettyDebug,
		ConfigDebug,
		DaemonDebug,
		Exception,
		DebugLog,
		DecodedData,
		FindNull,
		Error,
		FXMLLoader,
		Notification,
		ShutUp
	}
}

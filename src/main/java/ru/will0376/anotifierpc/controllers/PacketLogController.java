package ru.will0376.anotifierpc.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.controllers.utils.PacketLogsTableMouseHandler;
import ru.will0376.anotifierpc.net.RequestHandler;
import ru.will0376.anotifierpc.net.data.RequestData;
import ru.will0376.anotifierpc.utils.TableHelper;

public class PacketLogController extends ControllerInit {
	public static ObservableList<RequestData> requestData = FXCollections.observableArrayList();

	@FXML
	public TableView<RequestData> mainTable;
	@FXML
	public TableColumn<RequestData, Integer> id;
	@FXML
	public TableColumn<RequestData, String> time;
	@FXML
	public TableColumn<RequestData, String> action;
	@FXML
	public TableColumn<RequestData, Integer> port;
	@FXML
	public TableColumn<RequestData, String> uuid;
	@FXML
	public TableColumn<RequestData, String> ddata;

	Callback<TableColumn<RequestData, String>, TableCell<RequestData, String>> stringCellFactory = p -> {
		MyStringTableCell cell = new MyStringTableCell();
		cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new PacketLogsTableMouseHandler());
		return cell;
	};

	@Override
	public void postInit() {
		refillTable();
	}

	public void refillTable() {
		setUsersData();
		addAllToTable();
	}

	private void setUsersData() {
		requestData.clear();
		requestData.addAll(RequestHandler.requestDataList);
	}

	public void addAllToTable() {
		Platform.runLater(() -> {
			mainTable.setItems(FXCollections.observableArrayList());
			id.setCellValueFactory(new PropertyValueFactory<>("id"));
			time.setCellValueFactory(new PropertyValueFactory<>("time"));
			action.setCellValueFactory(new PropertyValueFactory<>("action"));
			port.setCellValueFactory(new PropertyValueFactory<>("port"));
			uuid.setCellValueFactory(new PropertyValueFactory<>("uuid"));
			uuid.setCellFactory(stringCellFactory);
			ddata.setCellValueFactory(new PropertyValueFactory<>("decryptedData"));
			ddata.setCellFactory(stringCellFactory);
			mainTable.setItems(requestData);
//			mainTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
			TableHelper.autoResizeColumns(mainTable);
		});
	}

	static class MyStringTableCell extends TableCell<RequestData, String> {

		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			setText(empty ? null : getString());
			setGraphic(null);
		}

		private String getString() {
			return getItem() == null ? "" : getItem();
		}
	}
}

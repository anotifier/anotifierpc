package ru.will0376.anotifierpc.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.net.ServerMessager;
import ru.will0376.anotifierpc.utils.TableHelper;
import ru.will0376.anotifierpc.utils.config.Config;
import ru.will0376.anotifierpc.utils.config.ConnectedDevice;

import java.net.URL;
import java.util.ResourceBundle;

public class DeviceNameListController extends ControllerInit {
	public static ObservableList<ConnectedDevice> deviceInfos = FXCollections.observableArrayList();
	@FXML
	public TextField nameField;
	public TableColumn<ConnectedDevice, Boolean> blocked;
	public TableColumn<ConnectedDevice, Boolean> device;
	@FXML
	TableView<ConnectedDevice> tableView;
	@FXML
	TableColumn<ConnectedDevice, String> id;
	@FXML
	TableColumn<ConnectedDevice, String> name;
	//TODO: Возможно доделать.
	/*public Timer timer;
	public TimerTask task = new TimerTask() {
		@Override
		public void run() {
			setUsersData();
			addAllToTable();
		}
	};*/

	private void setUsersData() {
		deviceInfos.clear();
		deviceInfos.addAll(ServerMessager.devices);
	}

	public void addAllToTable() {
		Platform.runLater(() -> {
			tableView.setItems(FXCollections.observableArrayList());
			id.setCellValueFactory(new PropertyValueFactory<>("uid"));
			name.setCellValueFactory(new PropertyValueFactory<>("savedName"));
			blocked.setCellValueFactory(new PropertyValueFactory<>("blocked"));
			device.setCellValueFactory(new PropertyValueFactory<>("device"));
			tableView.setItems(deviceInfos);
			TableHelper.autoResizeColumns(tableView);
		});
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		timer = new Timer();
//		timer.scheduleAtFixedRate(task, 0, 2000);
		refreshList();
	}

	public void refreshList() {
		setUsersData();
		addAllToTable();
	}

	public void setName() {
		String text = nameField.getText();
		if (text.isEmpty()) return;
		ConnectedDevice selectedItem = tableView.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			ConnectedDevice deviceByUID = config.getDeviceByUID(selectedItem.getUid());
			if (deviceByUID == null) {
				deviceByUID = ConnectedDevice.builder().build();
				selectedItem.copyTo(deviceByUID);
				config.getConnectedDevices().add(deviceByUID);
			}
			selectedItem.setSavedName(text);
			deviceByUID.setSavedName(text);

			config.saveConfig();
			nameField.setText("");
			refreshList();
		}
	}

	public void cleanSelected() {
//		MainController.appendToLogColor("Disabled.", Color.RED, MainController.LogStatus.FXMLLoader, MainController.LogStatus.Error);
		ConnectedDevice selectedItem = tableView.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			Config config = Main.config;
			selectedItem.setSavedName(null);
			ConnectedDevice deviceByUID = config.getDeviceByUID(selectedItem.getUid());
			deviceByUID.setSavedName(null);
			config.saveConfig();
			refreshList();
		}
	}

	public void toggleBlocked() {
		ConnectedDevice selectedItem = tableView.getSelectionModel().getSelectedItem();
		if (selectedItem != null) {
			ConnectedDevice deviceByUID = config.getDeviceByUID(selectedItem.getUid());
			if (deviceByUID == null) {
				deviceByUID = ConnectedDevice.builder().build();
				selectedItem.copyTo(deviceByUID);
				config.getConnectedDevices().add(deviceByUID);
			}
			deviceByUID.setBlocked(!deviceByUID.isBlocked());
			selectedItem.setBlocked(!deviceByUID.isBlocked());
			config.saveConfig();
			refreshList();
		}
	}
}

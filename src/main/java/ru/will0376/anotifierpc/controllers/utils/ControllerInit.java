package ru.will0376.anotifierpc.controllers.utils;

import javafx.fxml.Initializable;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.utils.config.Config;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

@Getter
@Setter
public abstract class ControllerInit implements Initializable {
	public MainController main = MainController.instance;
	public Config config = Main.config;
	public Stage stage;
	public Timer timer = new Timer();
	public TimerTask task;

	/**
	 * Имплементирует интерфейс. Стоит заметить, что переменная stage в этом методе недоступна
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * Метод вызывается сразу после инициализации класса.
	 */
	public void postInit() {
	}

	public void startTimer(long delay) {
		task = new TimerTask() {
			@Override
			public void run() {
				executeTask();
			}
		};
		timer.scheduleAtFixedRate(task, 0, delay);
	}

	public void startTimer() {
		startTimer(1000);
	}

	public void executeTask() {
	}

	public void reInit() {

	}
}

package ru.will0376.anotifierpc.controllers.utils;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.input.MouseEvent;
import ru.will0376.anotifierpc.controllers.Controllers;
import ru.will0376.anotifierpc.controllers.CopyValueController;
import ru.will0376.anotifierpc.net.data.RequestData;

public class PacketLogsTableMouseHandler implements EventHandler<MouseEvent> {
	@Override
	public void handle(MouseEvent event) {
		if (event.getTarget() instanceof TableCell<?, ?>) {
			TableCell<RequestData, String> target = (TableCell<RequestData, String>) event.getTarget();
			String text = target.getText();
			Platform.runLater(() -> {
				((CopyValueController) Controllers.CopyValueController.open().getInstance()).text.setText(text);
			});
		}
	}
}

package ru.will0376.anotifierpc.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.utils.Base64Util;
import ru.will0376.anotifierpc.utils.ExceptionHandler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Base64DecoderController extends ControllerInit {

	public TextArea base64in;
	public TextArea decodedOut;
	public CheckBox isImage;

	public static RenderedImage dataUrlToImage(String dataUrl) {
		byte[] bytes = dataUrl.getBytes();

		try {
			try (ByteArrayInputStream in = new ByteArrayInputStream(bytes)) {
				BufferedImage read = ImageIO.read(in);
				return read;
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ExceptionHandler.setThreadException();
	}

	private String decode(String in) {
		return Base64Util.decode(base64in.getText());
	}

	public void decodeBase64(ActionEvent actionEvent) {
		if (base64in.getText().isEmpty()) return;
		decodedOut.setText(decode(base64in.getText()));
	}

	public void saveDecodedToFile(ActionEvent actionEvent) {
		try {
			FileChooser fileChooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("All files", "*.*");
			fileChooser.getExtensionFilters().add(extFilter);
			File file = fileChooser.showSaveDialog(getStage());

			if (file != null) {
				if (!isImage.isSelected()) {
					saveTextToFile(decode(base64in.getText()), file);
				} else {
					byte[] bytes = decode(base64in.getText()).getBytes();
					OutputStream outStream = new FileOutputStream(file);
					outStream.write(bytes);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveTextToFile(String content, File file) {
		try (PrintWriter writer = new PrintWriter(file)) {
			writer.println(content);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}

package ru.will0376.anotifierpc.controllers;

import javafx.scene.control.Hyperlink;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class AboutController extends ControllerInit {
	public ImageView logo;
	public Hyperlink link;
	public Text version;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		version.setText(version.getText().replace("@ver@", Main.version));
		logo.setImage(Controllers.logo);
		link.setOnAction((e) -> {
			if (Desktop.isDesktopSupported()) {
				try {
					Desktop.getDesktop().browse(new URI("https://gitlab.com/anotifier/anotifierpc"));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}

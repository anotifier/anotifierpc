package ru.will0376.anotifierpc.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.utils.ExceptionHandler;
import ru.will0376.anotifierpc.utils.TextDebugLog;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class DebugLogController extends ControllerInit {
	public MainController mainControllerInstance;
	public Timer timer;
	@FXML
	TextFlow debugLog;
	public TimerTask task = new TimerTask() {
		@Override
		public void run() {
			if (mainControllerInstance.dirtyDebugLog) fillLog();
		}
	};
	@FXML
	TextField timerDelay;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainControllerInstance = MainController.instance;
		fillLog();
		timer = new Timer();
		timerDelay.setOnAction(e -> updateTimerSchedule());

		timerDelay.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) timerDelay.setText(newValue.replaceAll("[^\\d]", ""));
		});
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
		timerDelay.setText(String.valueOf(config.getDebugTimerDelay()));
		updateTimerSchedule();
	}

	public void updateTimerSchedule() {
		if (!timerDelay.getText().isEmpty()) {
			timer.cancel();
			timer = new Timer();
			timer.scheduleAtFixedRate(task, 0, Long.parseLong(timerDelay.getText()) * 1000);
			appendToDebug(TextDebugLog.builder()
					.text(String.format("Timer started with period: %s sec\n", timerDelay.getText()))
					.build());
			config.setDebugTimerDelay(Integer.parseInt(timerDelay.getText()));
			config.saveConfig();
		}
	}

	public void fillLog() {
		fillLog(true);
	}

	public void fillLog(boolean clearArray) {
		for (TextDebugLog textDebugLog : mainControllerInstance.debugLog) {
			appendToDebug(textDebugLog);
		}

		if (clearArray) mainControllerInstance.debugLog.clear();
		mainControllerInstance.dirtyDebugLog = false;
	}

	public void appendToDebug(TextDebugLog in) {
		Text text = new Text(in.getText());
		text.setFill(in.getColor());
		in.setPrinted(true);
		Platform.runLater(() -> {
			debugLog.getChildren().add(text);
		});
	}

	public void clearLog() {
		mainControllerInstance.debugLog.clear();
		debugLog.getChildren().clear();
		mainControllerInstance.dirtyDebugLog = false;
	}

	public void openCopyLog() {
		StringBuilder builder = new StringBuilder();
		for (Node child : debugLog.getChildren()) {
			Text child1 = (Text) child;
			String text = child1.getText();
			builder.append(text);
		}
		Controllers.ControllerStorage controllerStorage = Controllers.CopyValueController.open();
		CopyValueController instance = (CopyValueController) controllerStorage.getInstance();
		Platform.runLater(() -> {
			instance.text.setText(builder.toString());
		});
	}
}

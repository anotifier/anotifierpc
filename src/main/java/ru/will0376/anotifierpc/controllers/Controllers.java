package ru.will0376.anotifierpc.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lombok.*;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public enum Controllers {
	DebugLogController("DebugLog.fxml", "Debug Log"),
	DeviceNameList("DeviceNameList.fxml", "Device names"),
	PacketLog("PacketLog.fxml", "Log"),
	CopyValueController("CopyValue.fxml", "Value", true, -1, -1),
	NotificationFinder("NotificationFinder.fxml", "Notification Finder"),
	DecodeEncryptedDataController("DecodeEncryptedDataController.fxml", "Decode Encrypted"),
	Base64Decoder("Base64Decoder.fxml", "Base64 Decoder"),
	NotificationDefaultFormatting("NotificationDefaultFormatting.fxml", "Default Notification Formatting"),
	About("About.fxml", "About");

	public static final Image logo = new Image(String.valueOf(ClassLoader.getSystemResource("assets/logo.png")));
	private static final Map<Controllers, ControllerStorage> controllersMap = new HashMap<>();
	@NonNull String fxmlName;
	@NonNull String title;
	@NonNull boolean resize = false;
	@NonNull int width = -1;
	@NonNull int height = -1;

	public static ControllerStorage getControllerStorage(Controllers controller) {
		return controllersMap.getOrDefault(controller, null);
	}

	public static boolean isOpened(Controllers controller) {
		return controllersMap.getOrDefault(controller, null) != null;
	}

	public static ControllerStorage open(Controllers controller) {
		try {
			if (controllersMap.containsKey(controller)) {
				ControllerStorage controllerStorage = controllersMap.get(controller);
				controllerStorage.getStage().close();
			}
			URL resource = ClassLoader.getSystemResource("assets/fxml/" + controller.fxmlName);
			if (resource == null) {
				throw new NullPointerException(String.format("File %s not found", controller.fxmlName));
			}
			FXMLLoader fxmlLoader = new FXMLLoader(resource);
			//fxmlLoader.setResources(ResourceBundle.getBundle("ru.will0376.adbfx.Locales.Locale", Main.locale));
			Stage stage = new Stage();

			Parent root = fxmlLoader.load();

			stage.getIcons().add(logo);
			stage.setTitle(controller.getTitle());
			stage.setScene(new Scene(root, controller.getWidth(), controller.getHeight()));
			stage.initOwner(Main.primaryStage);
			stage.setResizable(controller.isResize());

			stage.show();
			ControllerStorage build = ControllerStorage.builder()
					.loader(fxmlLoader)
					.stage(stage)
					.instance(fxmlLoader.getController())
					.build();
			build.getInstance().setStage(stage);
			build.getInstance().postInit();
			controllersMap.put(controller, build);
			stage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, event -> {
				System.out.println(String.format("Removed %s from instance list", controller.name()));
				controllersMap.remove(controller);
			});
			return build;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ControllerStorage getControllerStorage() {
		return getControllerStorage(this);
	}

	public ControllerStorage open() {
		return open(this);
	}

	@Getter
	@Builder
	public static class ControllerStorage {
		private final ControllerInit instance;
		private final FXMLLoader loader;
		private final Stage stage;
	}
}

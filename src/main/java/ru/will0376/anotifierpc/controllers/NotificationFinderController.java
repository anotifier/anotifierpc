package ru.will0376.anotifierpc.controllers;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.commons.lang3.math.NumberUtils;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.utils.TableHelper;
import ru.will0376.anotifierpc.utils.config.NotificationFilter;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static ru.will0376.anotifierpc.utils.config.NotificationFilter.Flags.*;

public class NotificationFinderController extends ControllerInit {
	public static ObservableList<NotificationFilter> requestData = FXCollections.observableArrayList();

	public TableView<NotificationFilter> table;
	public TableColumn<NotificationFilter, Integer> id;
	public TableColumn<NotificationFilter, String> packageName;
	public TableColumn<NotificationFilter, String> flags;
	public TableColumn<NotificationFilter, String> context;
	public TextField idField;
	public TextField packageNameField;
	public TextField contextField;
	public CheckBox useRegex;
	public CheckBox anyIDs;
	public CheckBox useContext;
	public CheckBox useContextRegex;
	public CheckBox blocked;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		refillTable();
	}

	public void removeSelected(ActionEvent actionEvent) {
		TableView.TableViewSelectionModel<NotificationFilter> selectionModel = table.getSelectionModel();
		if (selectionModel != null && !selectionModel.isEmpty()) {
			NotificationFilter selectedItem = selectionModel.getSelectedItem();
			if (selectedItem != null) {
				config.getNotificationFilterList().remove(selectedItem);
				config.saveConfig();
				refillTable();
			}
		}
	}

	public void addNew(ActionEvent actionEvent) {
		if ((idField.getText().isEmpty() || !NumberUtils.isCreatable(idField.getText())) && !anyIDs.isSelected()) {
			return;
		}
		NotificationFilter notifyBuilder = NotificationFilter.builder()
				.packageName(packageNameField.getText())
				.id(!anyIDs.isSelected() ? Integer.parseInt(idField.getText()) : 0)
				.context(contextField.getText())
				.build();

		if (useContext.isSelected()) notifyBuilder.addFlag(UseContext);
		if (useContextRegex.isSelected()) notifyBuilder.addFlag(UseContextRegex);
		if (useRegex.isSelected()) notifyBuilder.addFlag(PackageNameRegex);
		if (anyIDs.isSelected()) notifyBuilder.addFlag(AnyIDs);
		if (blocked.isSelected()) notifyBuilder.addFlag(Blocked);

		config.getNotificationFilterList().add(notifyBuilder);
		config.saveConfig();
		refillTable();
	}

	public void refillTable() {
		setUsersData();
		addAllToTable();
	}

	private void setUsersData() {
		requestData.clear();
		requestData.addAll(config.getNotificationFilterList());
	}

	private void addAllToTable() {
		Platform.runLater(() -> {
			table.setItems(FXCollections.observableArrayList());
			id.setCellValueFactory(new PropertyValueFactory<>("id"));
			packageName.setCellValueFactory(new PropertyValueFactory<>("packageName"));
			context.setCellValueFactory(new PropertyValueFactory<>("context"));
			flags.setCellValueFactory(new PropertyValueFactory<>("flags"));
			flags.setCellValueFactory((e -> new SimpleStringProperty(e.getValue()
					.getFlagsList()
					.stream()
					.map(Enum::name)
					.collect(Collectors.joining(",")))));
			table.setItems(requestData);
			TableHelper.autoResizeColumns(table);
		});
	}
}

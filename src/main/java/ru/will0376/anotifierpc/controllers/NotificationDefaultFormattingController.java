package ru.will0376.anotifierpc.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.util.Duration;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.utils.NotificationBuilder;
import ru.will0376.anotifierpc.utils.TrayNotificationHelper;
import ru.will0376.anotifierpc.utils.config.NotificationFormatting;

import java.net.URL;
import java.util.ResourceBundle;

@EqualsAndHashCode(callSuper = true)
@Data
public class NotificationDefaultFormattingController extends ControllerInit {
	public ComboBox<ScreenItemWrapper> screenBox;
	public ComboBox<Pos> positionBox;
	public TextField duration;
	public NotificationFormatting formatting = config.getDefaultFormatting();
	public CheckBox darkMode;
	public CheckBox systemstyle;
	private ObservableList<ScreenItemWrapper> itemScreenList;

	public static ObservableList<ScreenItemWrapper> getItemScreenList() {
		ObservableList<ScreenItemWrapper> list = FXCollections.observableArrayList();
		int i = 0;
		for (Screen screen : Screen.getScreens()) {
			list.add(new ScreenItemWrapper(screen, ++i));
		}
		return list;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		duration.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				duration.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});

		duration.setText(String.valueOf((int) formatting.getHideAfterDuration().toSeconds()));

		itemScreenList = getItemScreenList();
		screenBox.setItems(itemScreenList);
		screenBox.getSelectionModel()
				.select(itemScreenList.stream()
						.filter(e -> e.getScreen().equals(formatting.getScreen()))
						.findFirst()
						.get());

		positionBox.setItems(FXCollections.observableArrayList(Pos.values()));
		positionBox.getSelectionModel().select(formatting.getPosition());

		darkMode.setSelected(formatting.isDarkMode());
		systemstyle.setSelected(formatting.isUseSystemNotifications());
	}

	public void save() {
		formatting.setHideAfterDuration(Duration.seconds(Double.parseDouble(duration.getText())));
		formatting.setPosition(positionBox.getValue());
		Screen value = screenBox.getValue().getScreen();
		formatting.setScreen(value);
		formatting.setScreenRectangle(value.getBounds());
		formatting.setDarkMode(darkMode.isSelected());
		formatting.setUseSystemNotifications(systemstyle.isSelected());
		config.saveConfig();
	}

	public void test() {
		NotificationBuilder notifications = NotificationBuilder.builder();
		notifications.owner(screenBox.getValue().getScreen());
		String test_notification = "Test Notification";
		notifications.title(test_notification);
		notifications.text(test_notification + " Text");
		notifications.position(positionBox.getValue());
		notifications.hideAfter(Duration.seconds(Double.parseDouble(duration.getText())));
		notifications.setDarkStyle(darkMode.isSelected());
		notifications.setSystemStay(systemstyle.isSelected());
		TrayNotificationHelper.showNotification(notifications);
	}

	@AllArgsConstructor
	@Data
	public static class ScreenItemWrapper {
		Screen screen;
		int localId;

		@Override
		public String toString() {
			Rectangle2D bounds = screen.getBounds();
			return String.format("%s. minX = %s, minY=%s, maxX=%s, maxY=%s, width=%s, height=%s", localId, bounds.getMinX(), bounds
					.getMinY(), bounds.getMaxX(), bounds.getMaxY(), bounds.getWidth(), bounds.getHeight());
		}
	}
}

package ru.will0376.anotifierpc.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;

import java.net.URL;
import java.util.ResourceBundle;

public class CopyValueController extends ControllerInit {
	@FXML
	public TextArea text;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		text.setWrapText(true);
	}
}

package ru.will0376.anotifierpc.controllers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ru.will0376.anotifierpc.controllers.utils.ControllerInit;
import ru.will0376.anotifierpc.utils.Base64Util;

import java.net.URL;
import java.util.ResourceBundle;

public class DecodeEncryptedDataController extends ControllerInit {
	public static JsonParser parser = new JsonParser();
	public TextArea encDataField;
	public TextArea outputData;
	public TextField clientKeyField;
	public CheckBox useMainKeyBox;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	public String getDecryptedData() {
		if (encDataField.getText().isEmpty()) {
			print("Enc Data is null");
		}
		try {
			String b64 = Base64Util.decode(encDataField.getText());
			JsonObject parse = parser.parse(b64).getAsJsonObject();
			return MainController.decryptText(useMainKeyBox.isSelected() ? MainController.instance.getClientEncKey() : MainController.instance
					.getEncKey(clientKeyField.getText()), Base64Util.decodeToByte(parse.get("encData")
					.getAsString()), Base64Util.decodeToByte(parse.get("iv").getAsString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public void decrypt(ActionEvent actionEvent) {
		print(getDecryptedData());
	}

	public void print(String in) {
		outputData.setText(in);
	}
}

package ru.will0376.anotifierpc.net.data;

import com.google.gson.JsonObject;
import lombok.Builder;
import lombok.Data;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.utils.Base64Util;

import java.util.Map;

@Data
@Builder
public class ResponseData {
	Action action;
	String encData;

	public enum Action {
		Pong
	}

	public static class ResponseDataBuilder {
		public ResponseDataBuilder encryptServerData() {
			try {
				Map<byte[], byte[]> map = MainController.encryptText(MainController.instance.getServerEncKey(), encData);
				map.forEach((key, value) -> {
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("iv", Base64Util.encode(key));
					jsonObject.addProperty("encData", Base64Util.encode(value));
					encData(Base64Util.encode(jsonObject.toString()));
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return this;
		}
	}
}

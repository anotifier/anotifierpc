package ru.will0376.anotifierpc.net.data;

import lombok.Data;
import lombok.Getter;
import ru.will0376.anotifierpc.net.RequestHandler;


@Data
public class RequestData {
	int id;
	Device device;
	String time;
	Action action;
	String encData;
	String decryptedData;
	String uuid;
	int port;

	@Getter
	public enum Action {
		Ping(new RequestHandler.HandlePing()),
		NewNotify(new RequestHandler.HandleNotify());
		RequestHandler.Handler handler;

		Action(RequestHandler.Handler handler) {
			this.handler = handler;
		}
	}

	public enum Device {
		Android,
		Mirror,
	}
}


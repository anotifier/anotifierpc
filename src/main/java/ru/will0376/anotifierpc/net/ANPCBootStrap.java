package ru.will0376.anotifierpc.net;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.net.decoder.RequestDecoder;
import ru.will0376.anotifierpc.net.decoder.ResponseDataEncoder;
import ru.will0376.anotifierpc.utils.ExceptionHandler;

public class ANPCBootStrap implements Runnable {
	public int port;
	public ServerBootstrap serverBootstrap;
	public ChannelFuture channelFuture;
	public EventLoopGroup bossGroup;
	public EventLoopGroup workerGroup;
	public ServerMessager messager;

	public ANPCBootStrap(int port) {
		this.port = port;
	}

	public void run() {
		try {
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
			MainController.appendToLog("Staring server", MainController.LogStatus.Netty);
			bossGroup = new NioEventLoopGroup();
			workerGroup = new NioEventLoopGroup();
			try {
				serverBootstrap = new ServerBootstrap();
				messager = new ServerMessager();
				serverBootstrap.group(bossGroup, workerGroup)
						.channel(NioServerSocketChannel.class)
						.childHandler(new ChannelInitializer<SocketChannel>() {
							@Override
							public void initChannel(SocketChannel ch) throws Exception {
								ch.pipeline()
										.addLast(new RequestDecoder(), new ResponseDataEncoder(), new ProcessingHandler(), messager);
							}
						})
						.option(ChannelOption.SO_BACKLOG, 128)
						.childOption(ChannelOption.SO_KEEPALIVE, true);

				channelFuture = serverBootstrap.bind(port).sync();
				MainController.appendToLog("Sync future!", MainController.LogStatus.Netty);
				channelFuture.channel().closeFuture().sync();
			} finally {
				stopAll();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void stopAll() {
		workerGroup.shutdownGracefully();
		bossGroup.shutdownGracefully();
		if (MainController.instance.bootStrapThread != null) MainController.instance.bootStrapThread.stop();
		MainController.appendToLog("Server stopped!", MainController.LogStatus.Netty);
		MainController.instance.checkDaemon();
	}
}

package ru.will0376.anotifierpc.net;

import ru.will0376.anotifierpc.controllers.MainController;

import java.util.concurrent.atomic.AtomicInteger;

public class NetStatusData {
	private final AtomicInteger counter = new AtomicInteger();

	public void increaseConnectionCounter() {
		synchronized (counter) {
			int newValue = counter.intValue() + 1;
			counter.set(newValue);
			updateCounterInUI();
		}
	}

	public void decreaseConnectionCounter() {
		synchronized (counter) {
			int newValue = counter.intValue() - 1;
			counter.set(newValue);
			updateCounterInUI();
		}
	}

	public int getActiveConnectionCounter() {
		return counter.get();
	}

	public void updateCounterInUI() {
		MainController.instance.connectionsCount.setText(String.valueOf(getActiveConnectionCounter()));
	}
}

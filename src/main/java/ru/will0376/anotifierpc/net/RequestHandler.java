package ru.will0376.anotifierpc.net;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.netty.channel.ChannelHandlerContext;
import javafx.scene.paint.Color;
import lombok.Data;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.Controllers;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.controllers.PacketLogController;
import ru.will0376.anotifierpc.net.data.RequestData;
import ru.will0376.anotifierpc.net.data.ResponseData;
import ru.will0376.anotifierpc.utils.*;
import ru.will0376.anotifierpc.utils.config.ConnectedDevice;
import ru.will0376.anotifierpc.utils.config.NotificationFilter;

import javax.crypto.BadPaddingException;
import java.lang.Exception;
import java.util.ArrayList;

import static ru.will0376.anotifierpc.controllers.MainController.LogStatus.Error;
import static ru.will0376.anotifierpc.controllers.MainController.LogStatus.Exception;
import static ru.will0376.anotifierpc.controllers.MainController.LogStatus.*;
import static ru.will0376.anotifierpc.utils.config.NotificationFilter.RefusedReason.CheckContext;
import static ru.will0376.anotifierpc.utils.config.NotificationFilter.RefusedReason.CheckIdPackage;

@Data
public class RequestHandler {
	public static ArrayList<RequestData> requestDataList = new ArrayList<>();
	public static JsonParser parser = new JsonParser();

	public static void handle(RequestData requestData) {
		new Thread(() -> {
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());

			requestDataList.add(requestData);
			MainController.appendToDebugLog(String.format("Handle data: %s", requestData.toString()), NettyDebug);
			Handler handler = requestData.getAction().getHandler().setData(requestData).decodeData();
			if (handler == null) {
				MainController.appendToLog("Handler is null, see log to more information", Netty, FindNull);
				return;
			}
			handler.process();
			if (Controllers.isOpened(Controllers.PacketLog)) {
				((PacketLogController) Controllers.getControllerStorage(Controllers.PacketLog)
						.getInstance()).refillTable();
			}
		}).start();
	}

	public static String checkStringIsNull(String in) {
		if (in == null || in.equals("null")) return "";
		return in;
	}

	public abstract static class Handler {
		RequestData requestDataIn;

		public abstract void process();

		public Handler setData(RequestData data) {
			requestDataIn = data;
			return this;
		}

		public RequestData getDataIn() {
			return requestDataIn;
		}

		public ConnectedDevice getDevice() {
			String deviceUID = requestDataIn.getUuid();
			ConnectedDevice deviceById = ServerMessager.findDeviceById(deviceUID);
			if (deviceById == null)
				MainController.appendToLog(String.format("Device with %s not found in map", deviceUID), Netty, FindNull);

			return deviceById;
		}

		public void sendBackMessage(ResponseData data) {
			ConnectedDevice device = getDevice();
			ServerMessager.staticSendMessage(data, device);
			MainController.appendToLog("Sended " + data.getAction() + " back", DebugLog);
		}

		public Handler decodeData() {
			try {
				String encData = requestDataIn.getEncData();
				String b64 = Base64Util.decode(encData);
				JsonObject parse = parser.parse(b64).getAsJsonObject();
				String s = MainController.decryptText(MainController.instance.getClientEncKey(), Base64Util.decodeToByte(parse
						.get("encData")
						.getAsString()), Base64Util.decodeToByte(parse.get("iv").getAsString()));
				requestDataIn.setDecryptedData(s);
				MainController.appendToDebugLog(requestDataIn.getDecryptedData(), Netty, DecodedData);
			} catch (Exception ex) {
				ExceptionHandler.catchException(ex);
				MainController.appendToLogColor(String.format("ERROR DECRYPTING PACKET WITH ID: %d, FROM DEVICE: %s (%s), REASON: %S", requestDataIn
						.getId(), requestDataIn.getUuid(), getKnownName(), ex.getLocalizedMessage()), Color.RED, Netty, DecodedData, Exception);
				if (ex instanceof BadPaddingException) {
					MainController.appendToLogColor("Tip: Check encryption keys", Color.DARKRED, Netty, DecodedData, Exception);
				}
				return null;
			}
			return this;
		}

		public String getKnownName() {
			return getKnownFullName(true);
		}

		public String getKnownFullName(boolean subtractUUID) {
			String uuid = requestDataIn.getUuid();
			String knownNameFromUID = Main.config.getKnownNameFromUID(uuid);
			return knownNameFromUID == null ? (subtractUUID ? uuid.substring(uuid.length() - 4) : uuid) : knownNameFromUID;
		}
	}

	public static class HandleNotify extends Handler {
		@Override
		public void process() {
			NotifyMessage message = NotifyMessage.getFromJson(getDataIn().getDecryptedData());
			JsonObject jo = parser.parse(getDataIn().getDecryptedData()).getAsJsonObject();
			NotificationFilter.ReturnResult returnResult = NotificationFilter.checkData(message.getId(), message.getPackageName());
			returnResult.setMessage(message);
			if (returnResult.getReason() == CheckIdPackage && returnResult.getFoundNotify().isBlocked()) return;
			returnResult.setJo(jo);
			if (returnResult.getReason() == CheckContext && NotificationFilter.checkContext(returnResult) && returnResult
					.getFoundNotify()
					.isBlocked()) return;

			MainController.appendToLog(String.format("Notify from %s, RemoteId: %s, App: %s, Data: %s", getDataIn().getUuid(),
					message.getId(), message.getAppLabel(), checkStringIsNull(message.getText())), Notification);

			TrayNotificationHelper.formattingAndShowNotify(NotificationBuilder.builder()
					.title(String.format("(%s) %s", getKnownName(), message.getAppLabel()))
					.text(returnResult.getFormattedText()));
		}
	}

	public static class HandlePing extends Handler {
		@Override
		public void process() {
			for (ConnectedDevice device : ServerMessager.devices) {
				if (device.getPort() == getDataIn().getPort()) {
					device.setUid(requestDataIn.getUuid());
					ConnectedDevice deviceByUID = Main.config.getDeviceByUID(getDataIn().getUuid());
					if (deviceByUID != null) {
						deviceByUID.copyTo(device);
						if (deviceByUID.isBlocked()) {
							ConnectedDevice deviceById = ServerMessager.findDeviceById(getDataIn().getUuid());
							MainController.appendToLogColor(String.format("Device %s is blocked", requestDataIn.getUuid()), Color.RED, Netty, Error);
							ChannelHandlerContext channel = deviceById.getChannel();
							if (channel != null) channel.close();
						}
					}
					break;
				}
			}

			MainController.appendToLog(String.format("Ping from %s (%s)", getDataIn().getUuid(), getKnownName()), Netty);
			TrayNotificationHelper.formattingAndShowNotify(NotificationBuilder.builder()
					.title(String.format("(%s) Connection", getKnownName()))
					.text(String.format("Ping from %s", getKnownFullName(false))));

			ResponseData build = ResponseData.builder()
					.action(ResponseData.Action.Pong)
					.encData("pong")
					.encryptServerData()
					.build();

			sendBackMessage(build);
		}
	}
}

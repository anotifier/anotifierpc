package ru.will0376.anotifierpc.net;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import javafx.scene.paint.Color;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.net.data.ResponseData;
import ru.will0376.anotifierpc.utils.ExceptionHandler;
import ru.will0376.anotifierpc.utils.config.ConnectedDevice;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

@ChannelHandler.Sharable
public class ServerMessager extends SimpleChannelInboundHandler<ResponseData> {
	public static List<ConnectedDevice> devices = new ArrayList<>();

	public ServerMessager() {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
	}

	public static void staticSendMessage(ResponseData data, ConnectedDevice device) {
		MainController.instance.bootStrap.messager.sendMessage(data, device);
	}

	public static ConnectedDevice findDeviceById(String in) {
		return devices.stream().filter(e -> e.getUid().equals(in)).findFirst().orElse(null);
	}

	public void sendMessage(ResponseData msgToSend, ConnectedDevice device) {
		ChannelHandlerContext ctx = device.getChannel();
		if (ctx == null) {
			MainController.appendToLogColor(String.format("Channel for %s is null", device.getUid()), Color.RED, MainController.LogStatus.Error);
		}

		if (ctx != null && !ctx.isRemoved() && ctx.channel().isActive()) {
			ChannelFuture cf = ctx.write(msgToSend);
			ctx.flush();
			if (!cf.isSuccess()) {
				if (cf.cause() != null) //We are trying to correct false positives.
					MainController.appendToLogColor(String.format("send failed: %s", cf.cause()
							.getMessage()), Color.RED, MainController.LogStatus.Error);
			}
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		InetSocketAddress inetSocketAddress = (InetSocketAddress) ctx.channel().remoteAddress();

		ConnectedDevice build = ConnectedDevice.builder()
				.address(inetSocketAddress.getAddress())
				.port(inetSocketAddress.getPort())
				.build();
		build.setChannel(ctx);
		devices.add(build);
		MainController.netStatusData.increaseConnectionCounter();
		MainController.appendToDebugLog(String.format("Channel %s is now active", ctx.channel()), MainController.LogStatus.Netty);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, ResponseData msg) throws Exception {

	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		InetSocketAddress inetSocketAddress = (InetSocketAddress) ctx.channel().remoteAddress();
		ConnectedDevice device = null;
		for (ConnectedDevice connectedDevice : devices) {
			if (connectedDevice.getPort() == inetSocketAddress.getPort()) device = connectedDevice;
		}
		if (device != null) devices.remove(device);
		super.channelInactive(ctx);

		MainController.netStatusData.decreaseConnectionCounter();
		MainController.appendToDebugLog(String.format("Channel %s is now inactive", ctx.channel()), MainController.LogStatus.Netty);
	}
}

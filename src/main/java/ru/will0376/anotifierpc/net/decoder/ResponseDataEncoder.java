package ru.will0376.anotifierpc.net.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import ru.will0376.anotifierpc.net.data.ResponseData;
import ru.will0376.anotifierpc.utils.ExceptionHandler;

import java.nio.charset.StandardCharsets;

public class ResponseDataEncoder extends MessageToByteEncoder<ResponseData> {
	public ResponseDataEncoder() {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.close();
		ExceptionHandler.catchException(cause);
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, ResponseData msg, ByteBuf out) throws Exception {
		out.writeInt(msg.getAction().ordinal());
		out.writeInt(msg.getEncData().length());
		out.writeCharSequence(msg.getEncData(), StandardCharsets.UTF_8);
	}
}

package ru.will0376.anotifierpc.net.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import ru.will0376.anotifierpc.controllers.MainController;
import ru.will0376.anotifierpc.net.data.RequestData;
import ru.will0376.anotifierpc.utils.ExceptionHandler;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class RequestDecoder extends ReplayingDecoder<RequestData> {
	private final Charset charset = StandardCharsets.UTF_8;

	public RequestDecoder() {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.close();
		ExceptionHandler.catchException(cause);
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		RequestData data = new RequestData();
		data.setId(in.readInt());
		data.setDevice(RequestData.Device.values()[in.readInt()]);
		data.setTime(MainController.timeFormat.format(System.currentTimeMillis()));
		data.setAction(RequestData.Action.values()[in.readInt()]);
		data.setEncData(readString(in));
		data.setUuid(readString(in));
		data.setPort(((InetSocketAddress) ctx.channel().remoteAddress()).getPort());
		out.add(data);
	}

	public String readString(ByteBuf in) {
		return in.readCharSequence(in.readInt(), charset).toString();
	}
}

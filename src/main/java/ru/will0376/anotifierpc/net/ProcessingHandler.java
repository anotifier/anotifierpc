package ru.will0376.anotifierpc.net;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import ru.will0376.anotifierpc.net.data.RequestData;

public class ProcessingHandler extends ChannelInboundHandlerAdapter {
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		RequestHandler.handle((RequestData) msg);
		super.channelRead(ctx, msg);
	}
}

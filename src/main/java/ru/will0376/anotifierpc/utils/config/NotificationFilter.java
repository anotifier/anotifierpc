package ru.will0376.anotifierpc.utils.config;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.utils.NotifyMessage;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class NotificationFilter {
	public static String[] checkContextIds = new String[]{"nt.summaryText", "nt.infoText", "nt.curTextLast", "nt.text"};
	@Expose
	@Builder.Default
	public List<Flags> flagsList = new ArrayList<>();
	@Expose
	int id;
	@Expose
	String packageName;
	@Expose
	@Builder.Default
	String context = "";

	/**
	 * Проверяет наличие id и packageName в конфиге с учетом флагов
	 *
	 * @return true - если сходятся условия
	 */
	public static ReturnResult checkData(int id, String packageName) {
		for (NotificationFilter notify : Main.config.getNotificationFilterList()) {

			boolean checkId = notify.getFlagsList().contains(Flags.AnyIDs);

			if (!checkId && id == notify.getId()) checkId = true;

			boolean checkPackage = notify.getFlagsList()
					.contains(Flags.PackageNameRegex) && packageName.matches(notify.getPackageName());

			if (!checkPackage && packageName.equals(notify.getPackageName())) checkPackage = true;

			if (checkId && checkPackage) {

				if (notify.getFlagsList().contains(Flags.UseContext) && notify.getFlagsList()
						.contains(Flags.UseContextRegex))
					return ReturnResult.builder().foundNotify(notify).reason(RefusedReason.CheckContext).build();

				return ReturnResult.builder().foundNotify(notify).reason(RefusedReason.CheckIdPackage).build();
			}
		}
		return ReturnResult.builder().reason(RefusedReason.None).build();
	}

	/**
	 * Проверяет контекст из уведомления
	 *
	 * @return true при совпадении контекста.
	 */
	public static boolean checkContext(ReturnResult result) {
		JsonObject jo = result.getJo();
		if (result.getReason() != RefusedReason.CheckContext) {
			return false;
		}

		for (String s : checkContextIds) {
			if (jo.has(s)) {
				String asString = jo.get(s).getAsString();
				String context = result.getFoundNotify().context;
				if (result.getFoundNotify().getFlagsList().contains(Flags.UseContext)) {
					if (asString.contains(context)) return true;
				} else if (result.getFoundNotify().getFlagsList().contains(Flags.UseContextRegex)) {
					if (asString.matches(context)) return true;
				}
			}
		}
		return false;
	}

	public static NotificationFilter findFilterByID(String id) {
		return Main.config.getNotificationFilterList()
				.stream()
				.filter(e -> e.getIdentifier().equals(id))
				.findFirst()
				.orElse(null);
	}

	public void addFlag(Flags flags) {
		this.flagsList.add(flags);
	}

	public boolean isBlocked() {
		return flagsList.contains(Flags.Blocked);
	}

	public String getIdentifier() {
		return String.format("%s,%s,%s", id, packageName, context);
	}

	@Getter
	public enum Flags {
		PackageNameRegex("Use Regex in package name"),
		AnyIDs("Any ID's"),
		UseContext("Use context"),
		UseContextRegex("Use regex for context"),
		Blocked,
		Formatting // Maybe in the future.
		;
		String normalName;

		Flags(String normalName) {
			this.normalName = normalName;
		}

		Flags() {
			normalName = this.name();
		}
	}

	@Getter
	public enum RefusedReason {
		CheckIdPackage,
		CheckContext,
		None
	}

	@Getter
	@Builder
	@Setter
	public static class ReturnResult {
		RefusedReason reason;
		NotificationFilter foundNotify;
		JsonObject jo;
		NotifyMessage message;

		public String getFormattedText() {
			if (foundNotify == null) return message.getTitle() + ": " + message.getText();
			return foundNotify.getFlagsList()
					.contains(Flags.Formatting) ? "" : message.getTitle() + ": " + message.getText();
		}
	}
}


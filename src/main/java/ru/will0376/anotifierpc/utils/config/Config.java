package ru.will0376.anotifierpc.utils.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonReader;
import javafx.geometry.Rectangle2D;
import javafx.util.Duration;
import lombok.Data;
import ru.will0376.anotifierpc.utils.config.adapter.DurationAdapter;
import ru.will0376.anotifierpc.utils.config.adapter.NotificationFormattingAdapter;
import ru.will0376.anotifierpc.utils.config.adapter.Rectangle2DAdapter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

@Data
public class Config {
	public static final Gson gson = new GsonBuilder().setPrettyPrinting()
			.excludeFieldsWithoutExposeAnnotation()
			.registerTypeAdapter(Rectangle2D.class, new Rectangle2DAdapter())
			.registerTypeAdapter(Duration.class, new DurationAdapter())
			.registerTypeAdapter(NotificationFormatting.class, new NotificationFormattingAdapter())
			.create();
	public static File configFile;

	@Expose
	public int port = 2015;
	@Expose
	public boolean autoStart = false;
	@Expose
	public String serverSecretKey = "<Insert Key>";
	@Expose
	public String clientSecretKey = "<Insert Key>";
	@Expose
	public int debugTimerDelay = 1;
	@Expose
	public List<ConnectedDevice> connectedDevices = new ArrayList<>();
	@Expose
	public List<NotificationFilter> NotificationFilterList = new ArrayList<>();
	@Expose
	public NotificationFormatting defaultFormatting = new NotificationFormatting().init();

	public static Config load(File file) throws Exception {
		configFile = file;
		if (file.exists()) {
			try {
				if (file.length() > 2) return gson.fromJson(new JsonReader(new FileReader(file)), Config.class);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		Config config = new Config();
		config.saveConfig(file);
		return config;
	}

	public String getCfgInString() {
		return gson.toJson(this);
	}

	public void saveConfig(File file) throws Exception {
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter f2 = new FileWriter(file, false);
		f2.write(getCfgInString());
		f2.close();
	}

	public void saveConfig() {
		try {
			saveConfig(configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getKnownNameFromUID(String in) {
		ConnectedDevice deviceByUID = getDeviceByUID(in);
		if (deviceByUID != null) return deviceByUID.getSavedName();

		return null;
	}

	/**
	 * @deprecated Использовать только для хранения данных в конфиге!
	 */
	@Deprecated
	public ConnectedDevice getDeviceByUID(String in) {
		return connectedDevices.stream().filter(e -> e.getUid().equals(in)).findFirst().orElse(null);
	}
}

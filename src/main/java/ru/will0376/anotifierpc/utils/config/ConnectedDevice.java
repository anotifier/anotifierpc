package ru.will0376.anotifierpc.utils.config;

import com.google.gson.annotations.Expose;
import io.netty.channel.ChannelHandlerContext;
import lombok.Builder;
import lombok.Data;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.net.data.RequestData;

import java.net.InetAddress;

@Data
@Builder
public class ConnectedDevice {
	InetAddress address;
	int port;
	@Expose
	String uid;
	@Expose
	String savedName;
	ChannelHandlerContext channel;
	@Expose
	@Builder.Default
	boolean blocked = false;
	@Expose
	@Builder.Default
	RequestData.Device device = RequestData.Device.Android;

	public void setKnownName() {
		setSavedName(Main.config.getKnownNameFromUID(uid));
	}

	public void copyTo(ConnectedDevice in) {
		in.uid = uid;
		in.savedName = savedName;
		in.blocked = blocked;
		in.device = device;
	}

	public static class ConnectedDeviceBuilder {
		public ConnectedDeviceBuilder setKnownName() {
			savedName(Main.config.getKnownNameFromUID(uid));
			return this;
		}
	}
}

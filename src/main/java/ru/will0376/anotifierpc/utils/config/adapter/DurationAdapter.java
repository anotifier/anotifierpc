package ru.will0376.anotifierpc.utils.config.adapter;

import com.google.gson.*;
import javafx.util.Duration;

import java.lang.reflect.Type;

public class DurationAdapter implements JsonDeserializer<Duration>, JsonSerializer<Duration> {
	@Override
	public Duration deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		double duration = json.getAsJsonObject().get("duration").getAsInt();
		return Duration.seconds(duration);
	}

	@Override
	public JsonElement serialize(Duration src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject jo = new JsonObject();
		jo.addProperty("duration", src.toSeconds());
		return jo;
	}
}

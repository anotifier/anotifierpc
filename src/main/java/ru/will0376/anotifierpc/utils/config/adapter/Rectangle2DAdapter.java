package ru.will0376.anotifierpc.utils.config.adapter;

import com.google.gson.*;
import javafx.geometry.Rectangle2D;

import java.lang.reflect.Type;

public class Rectangle2DAdapter implements JsonDeserializer<Rectangle2D>, JsonSerializer<Rectangle2D> {
	@Override
	public Rectangle2D deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jo = json.getAsJsonObject();
		double minX = jo.get("minX").getAsDouble();
		double minY = jo.get("minY").getAsDouble();
		double width = jo.get("width").getAsDouble();
		double height = jo.get("height").getAsDouble();
		return new Rectangle2D(minX, minY, width, height);
	}

	@Override
	public JsonElement serialize(Rectangle2D src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject jo = new JsonObject();
		jo.addProperty("minX", src.getMinX());
		jo.addProperty("minY", src.getMinY());
		jo.addProperty("width", src.getWidth());
		jo.addProperty("height", src.getHeight());
		return jo;
	}
}

package ru.will0376.anotifierpc.utils.config.adapter;

import com.google.gson.*;
import com.google.gson.annotations.Expose;
import lombok.SneakyThrows;
import ru.will0376.anotifierpc.utils.config.NotificationFormatting;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

public class NotificationFormattingAdapter implements JsonSerializer<NotificationFormatting>, JsonDeserializer<NotificationFormatting> {

	@SneakyThrows
	@Override
	public JsonElement serialize(NotificationFormatting form, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject jo = new JsonObject();

		for (Field field : form.getClass().getDeclaredFields()) {
			Expose annotation = field.getAnnotation(Expose.class);
			if (annotation != null && annotation.serialize()) {
				field.setAccessible(true);
				jo.add(field.getName(), context.serialize(field.get(form)));
			}
		}
		return jo;
	}

	@SneakyThrows
	@Override
	public NotificationFormatting deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		NotificationFormatting notificationFormatting = new NotificationFormatting();
		JsonObject jo = json.getAsJsonObject();
		for (Field field : notificationFormatting.getClass().getDeclaredFields()) {
			Expose annotation = field.getAnnotation(Expose.class);
			if (annotation != null && annotation.deserialize()) {
				field.setAccessible(true);
				field.set(notificationFormatting, context.deserialize(jo.get(field.getName()), field.getType()));
			}
		}
		notificationFormatting.init();
		return notificationFormatting;
	}
}

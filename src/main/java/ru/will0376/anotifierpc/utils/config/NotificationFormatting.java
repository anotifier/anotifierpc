package ru.will0376.anotifierpc.utils.config;

import com.google.gson.annotations.Expose;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.util.Duration;
import lombok.Data;
import ru.will0376.anotifierpc.utils.NotificationBuilder;

@Data
public class NotificationFormatting {
	@Expose
	private Pos position = Pos.BOTTOM_RIGHT;
	@Expose
	private Duration hideAfterDuration = Duration.seconds(5);
	private Screen screen;
	@Expose
	private Rectangle2D screenRectangle = Rectangle2D.EMPTY;
	@Expose
	private boolean darkMode = false;
	@Expose
	private String notificationId = "default";
	@Expose
	private boolean useSystemNotifications = false;

	public NotificationFormatting init() {
		try {
			ObservableList<Screen> screensForRectangle = Screen.getScreensForRectangle(screenRectangle);
			screen = !screensForRectangle.isEmpty() ? screensForRectangle.get(0) : Screen.getPrimary();
		} catch (Exception ex) {
			ex.printStackTrace();
			screen = Screen.getPrimary();
		}
		return this;
	}

	//TODO: Может быть сделаю для перезагружающих уведомлений.
	public void applyFormatting(NotificationBuilder notifications) {
		notifications.owner(screen)
				.position(position)
				.hideAfter(hideAfterDuration)
				.setDarkStyle(darkMode)
				.setSystemStay(useSystemNotifications);
	}
}

package ru.will0376.anotifierpc.utils;


import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Util {
	public static String encode(String in) {
		return Base64.getEncoder().encodeToString(in.getBytes(StandardCharsets.UTF_8));
	}

	public static String encode(byte[] in) {
		return Base64.getEncoder().encodeToString(in);
	}

	public static String decode(String in) {
		try {
			return new String(new BASE64Decoder().decodeBuffer(in));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] decodeToByte(String in) {
		try {
			return new BASE64Decoder().decodeBuffer(in);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}

package ru.will0376.anotifierpc.utils;

import ru.will0376.anotifierpc.controllers.MainController;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

	public static void setThreadException() {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
	}

	public static void catchException(Thread thread, Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		throwable.printStackTrace(pw);

		if (throwable instanceof ThreadDeath) {
			MainController.appendToDebugLog(String.format("Thread %s is dead, stacktrace: \n%s", thread.getName(), sw), MainController.LogStatus.Daemon);
		} else {
			throwable.printStackTrace();
			MainController.appendToDebugLog(String.format(" on %s -> %s", thread.getName(), sw), MainController.LogStatus.Exception);
		}
	}

	public static void catchException(Throwable throwable) {
		catchException(Thread.currentThread(), throwable);
	}

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		catchException(t, e);
	}
}

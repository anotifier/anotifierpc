package ru.will0376.anotifierpc.utils;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.Notifications;
import ru.will0376.anotifierpc.Main;

public class TrayNotificationHelper {
	public static long shutTime = 0;

	public static void formattingAndShowNotify(NotificationBuilder builder) {
		Main.config.getDefaultFormatting().applyFormatting(builder);
		showNotification(builder);
	}

	public static void showNotification(NotificationBuilder notification) {
		if (canShow()) {
			if (!Main.primaryStage.isShowing())
				new JFXPanel();
			Platform.runLater(() -> {
				try {
					if (!Main.primaryStage.isShowing()) {
						createDummyStage();
						System.out.println("Create a transparent window");
					}
					if (!notification.isSystemTray()) {
						Notifications notifications = Notifications.create()
								.title(notification.getTitle())
								.owner(notification.getScreen())
								.hideAfter(notification.getHideAfter())
								.text(notification.getText());
						if (notification.isDarkMode())
							notifications.darkStyle();
						notifications.showInformation();
					} else {
						Tray.instance.drawNotification(notification);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	public static boolean canShow() {
		return System.currentTimeMillis() > shutTime;
	}

	private static void createDummyStage() {
		Stage dummyPopup = new Stage();
		dummyPopup.initModality(Modality.NONE);
		dummyPopup.initStyle(StageStyle.UTILITY);
		dummyPopup.setOpacity(0d);
		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();
		dummyPopup.setX(bounds.getMaxX());
		dummyPopup.setY(bounds.getMaxY());
		Group root = new Group();
		dummyPopup.setScene(new Scene(root, 1d, 1d, Color.TRANSPARENT));
		dummyPopup.show();
	}
}

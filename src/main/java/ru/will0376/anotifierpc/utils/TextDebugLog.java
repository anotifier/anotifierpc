package ru.will0376.anotifierpc.utils;

import javafx.scene.paint.Color;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TextDebugLog {
	String text;
	@Builder.Default
	Color color = Color.BLACK;
	@Builder.Default
	boolean printed = false;

	public void togglePrinted() {
		printed = !printed;
	}
}

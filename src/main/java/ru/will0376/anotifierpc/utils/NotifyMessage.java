package ru.will0376.anotifierpc.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class NotifyMessage {
	public static volatile Gson gson = new GsonBuilder().setPrettyPrinting().create();
	@SerializedName("sbn.id")
	int id;
	@SerializedName("sbn.tag")
	String tag;
	@SerializedName("sbn.packageName")
	String packageName;
	@SerializedName("nt.time")
	int time;
	@SerializedName("nt.flags")
	int flags;
	@SerializedName("nt.priority")
	int priority;
	@SerializedName("nt.extras")
	int extras;
	@SerializedName("nt.appLabel")
	String appLabel;
	@SerializedName("nt.tickerText")
	String tickerText;
	@SerializedName("nt.title")
	String title;
	@SerializedName("nt.text")
	String text;
	@SerializedName("nt.actions")
	String actions;
	@SerializedName("nt.infoText")
	String infoText;
	@SerializedName("nt.curTextLast")
	String curTextLast;
	@SerializedName("nt.summaryText")
	String summaryText;
	@SerializedName("nt.hasPic")
	boolean hasPic;
	@SerializedName("nt.hasImage")
	boolean hasImage;


	public static NotifyMessage getFromJson(String in) {
		return gson.fromJson(new JsonParser().parse(in), NotifyMessage.class);
	}

	public String getString() {
		return gson.toJson(this);
	}
}

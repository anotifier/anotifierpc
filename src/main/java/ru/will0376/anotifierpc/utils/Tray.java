package ru.will0376.anotifierpc.utils;

import javafx.application.Platform;
import ru.will0376.anotifierpc.Main;
import ru.will0376.anotifierpc.controllers.MainController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class Tray {
	public static Tray instance;
	public static TrayIcon trayIcon;
	public boolean minimize = false;
	public void newTray() {
		try {
			instance = this;
			if (!SystemTray.isSupported()) {
				System.out.println("System tray is not supported !!! ");
				return;
			}

			BufferedImage trayIconImage = ImageIO.read(ClassLoader.getSystemResource("assets/logo.png"));
			PopupMenu trayPopupMenu = new PopupMenu();

			MenuItem action = new MenuItem("Show");
			action.addActionListener(e -> {
				if (Main.primaryStage.isShowing()) {
					minimize = true;
					Platform.runLater(() -> Main.primaryStage.hide());
				} else {
					minimize = false;
					Platform.runLater(() -> Main.primaryStage.show());
				}
			});
			trayPopupMenu.add(action);

			MenuItem startDaemon = new MenuItem("Start daemon");
			startDaemon.addActionListener(e -> MainController.instance.startDaemon());
			trayPopupMenu.add(startDaemon);

			MenuItem stopDaemon = new MenuItem("Stop daemon");
			stopDaemon.addActionListener(e -> MainController.instance.stopDaemon());
			trayPopupMenu.add(stopDaemon);

			MenuItem close = new MenuItem("Exit");
			close.addActionListener(e -> Runtime.getRuntime().halt(0));
			trayPopupMenu.add(close);

			trayIcon = new TrayIcon(trayIconImage, "ANotifierPC", trayPopupMenu);
			trayIcon.setImageAutoSize(true);
			trayIcon.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(java.awt.event.MouseEvent e) {
					if (e.getButton() == 1) {
						if (Main.primaryStage.isShowing()) {
							minimize = true;
							Platform.runLater(() -> Main.primaryStage.hide());
						} else {
							minimize = false;
							Platform.runLater(() -> Main.primaryStage.show());
						}
					}
				}

				@Override
				public void mousePressed(java.awt.event.MouseEvent e) {

				}

				@Override
				public void mouseReleased(java.awt.event.MouseEvent e) {

				}

				@Override
				public void mouseEntered(java.awt.event.MouseEvent e) {

				}

				@Override
				public void mouseExited(java.awt.event.MouseEvent e) {

				}
			});
			SystemTray systemTray = SystemTray.getSystemTray();
			systemTray.add(trayIcon);
		} catch (Exception awtException) {
			awtException.printStackTrace();
		}
	}

	public void drawNotification(NotificationBuilder notification) {
		trayIcon.displayMessage(notification.getTitle(), notification.getText(), TrayIcon.MessageType.INFO);
	}
}

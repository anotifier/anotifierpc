package ru.will0376.anotifierpc.utils;

import javafx.geometry.Pos;
import javafx.stage.Screen;
import javafx.util.Duration;
import lombok.Getter;

@Getter
public class NotificationBuilder {
	private String text;
	private String title;
	private Screen screen;
	private Pos position;
	private Duration hideAfter;
	private boolean darkMode = false;
	private boolean systemTray = false;

	private NotificationBuilder() {
	}

	public static NotificationBuilder builder() {
		return new NotificationBuilder();
	}

	public NotificationBuilder owner(Screen owner) {
		this.screen = owner;
		return this;
	}

	public NotificationBuilder hideAfter(Duration duration) {
		this.hideAfter = duration;
		return this;
	}

	public NotificationBuilder position(Pos position) {
		this.position = position;
		return this;
	}

	public NotificationBuilder text(String text) {
		this.text = text;
		return this;
	}

	public NotificationBuilder title(String title) {
		this.title = title;
		return this;
	}

	public NotificationBuilder darkStyle() {
		darkMode = true;
		return this;
	}

	public NotificationBuilder systemStay() {
		systemTray = true;
		return this;
	}

	public NotificationBuilder setDarkStyle(boolean in) {
		darkMode = in;
		return this;
	}

	public NotificationBuilder setSystemStay(boolean in) {
		systemTray = in;
		return this;
	}
}
